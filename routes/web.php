<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $products = App\Product::paginate(4);
    return view('welcome', compact('products'));
});

Auth::routes();
// Route::get('/home','HomeController')
//, 'middleware' => ['admin','auth']
Route::group(['prefix' => 'admin'], function(){
Route::resource('/','AdminController');
Route::resource('/home','AdminController');
Route::resource('/product','ProductesController');
Route::resource('/category','CateroriesController');

});

Route::get('/products','ProductesController@allproducts')->name('allproducts');























//***************************************************************//
//-------------------USER---------------------------------------//
Route::resource('/user','UserController');
Route::get('/user','UserController@index');
Route::post('/user/create','UserController@store');
Route::any('/user/destroy/{id}','TaskController@destroy');
Route::get('/user/{id}/edit','TaskController@edit');
Route::Post('/user/update','TaskController@update');
//Route::get('','')

//***********************************************************//
//--------------------TASK-----------------------------------//
Route::get('/task','TaskController@index');
//Route::resource('/task','TaskController');
Route::any('/task/destroy/{id}','TaskController@destroy');
Route::Post('/task/gadda','TaskController@update');
Route::get('/task/{id}/edit','TaskController@edit');
Route::get('/task/search','TaskController@');






