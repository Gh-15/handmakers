<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Http\Controllers\Input;
class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::get();
        return view('task.index',compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('task.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task();
        $task->name = $request->get('name');
        $task->description = $request->get('description');
        $task->done = $request->get('done');
        $task->wight = $request->get('wight');
        $task->User_Id = $request->get('userid');
        $task->save();
        return redirect ()->action("TaskController@index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dd($id);
        $tasks = Task::find($id);
        return view('task.edit',compact('tasks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd("asd");
        $id = $request->get('id');
        $tasks = Task::find($id);
        $tasks->name=$request->get('name');
        $tasks->description=$request->get('description');
        $tasks->done=$request->get('done');
        $tasks->wight=$request->get('wight');
        $tasks->User_Id=$request->get('userid');
        $tasks->save();
        return redirect ()->action("TaskController@index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $tasks = Task::findOrFail($id);
        $tasks->delete();

        return redirect ()->action("TaskController@index");
    }
}
