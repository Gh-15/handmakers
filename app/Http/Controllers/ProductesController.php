<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use\App\Product;
use Storage;
use App\Category;

class ProductesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::paginate(5);
        return view('admin.product.index', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.product.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'title' => 'required',
            'price' => 'required',
            'description' => 'required|numeric',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif',
        ]);


        $product = new Product();
        $product->title = $request->get('Title');
        $product->description = $request->get('Description');
        //$product->image = $request->get();
        $product->price = $request->get('Price');
        $product->Category_Id = $request->get('category_id');
        if($request->hasFile('Image')){
            $file = $request->file('Image');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $location = public_path('/images');
            $file->move($location,$filename);
            $product->image = $filename;

        }
        $product->save();
        return back()->withInfo('product successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $product = Product::find($id);
        return view('admin.product.edit', compact('product','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $request->validate([
        //     'title' => 'required',
        //     'price' => 'required',
        //     'description' => 'required|numeric',
        //     'image' => 'required|image|mimes:jpeg,png,jpg,gif',
        // ]);


        $product = Product::find($id);
        $product->title = $request->get('Title');
        $product->description = $request->get('Description');
        //$product->image = $request->get();
        $product->price = $request->get('Price');
        if($request->hasFile('Image')){
            $file = $request->file('Image');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $location = public_path('/images');
            $file->move($location,$filename);
            $oldImage = $product->image;
            \Storage::delete($oldImage);
            $product->image = $filename;

        }
        $product->save();
        return back()->withInfo('product successfully update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        Storage::delete($product->image);
        $product->delete();
        return back()->withInfo('product successfully deleted');
    }

    public function allproducts()
    {
        $products = Product::paginate(5);
        return view('allproduct', compact('products'));
    }
}
