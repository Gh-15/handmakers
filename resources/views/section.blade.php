<h1>Section</h1>
<table border="3">
    <tr>
        <td>id</td>
        <td>Name</td>
        <td>Body</td>
    </tr>
@foreach($sections as $section)
    <tr>
        <td>{{$section->id}}</td>
        <td>{{$section->name}}</td>
        <td>{{$section->body}}</td>
    </tr>
@endforeach

</table>